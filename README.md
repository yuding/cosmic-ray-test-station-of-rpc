# Cosmic ray test station of RPC


# NORMAL Mode 
** 8 channels for one FE board **

NormalMode_allChannels_Pre.py :   Build the TTree and save all the information of the '.csv' files by using the vectors

NormalMode_allChannels_Ana.py :   Further analysis by using the TFile and TTree


# DEBUG Mode 
** Only one channel for the debug **

DebugMode_OneChannel_Pre.py :   Build the TTree and save all the information of the '.csv' files by using the vectors

DebugMode_OneChannel_Ana.py :   Further analysis by using the TFile and TTree


# Usage
** For 'NormalMode_allChannels_Pre.py' and 'DebugMode_OneChannel_Pre.py' **

python3 DebugMode_OneChannel_Pre.py -i DataFolder/1MHz_W10ns_241MHz -o 1MHz_W10ns_241MHz_Out

** For 'NormalMode_allChannels_Ana.py' and 'DebugMode_OneChannel_Ana.py' **

python3 DebugMode_OneChannel_Ana.py -i 1MHz_W10ns_241MHz_Out
