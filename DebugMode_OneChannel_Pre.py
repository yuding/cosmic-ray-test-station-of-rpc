# -*- coding: utf-8 -*-
from ROOT import *
import csv
from array import array
import os
import argparse
import numpy as np

# This code is only for one channel : Ch4
# use argparse to set the configuration
parser = argparse.ArgumentParser(description="Read the DAQ data")
parser.add_argument("-i","--inputDir", required=True, help="Directory that contains input files to loop over.")
parser.add_argument("-o","--outputDir", required=True, help="Directory that save the Rootfile.")

args = parser.parse_args() 

DirPath = os.getcwd()
Absolute_DirPath = os.path.abspath(DirPath) + '/'

# Set OutputFolder and enter the folder
if not os.path.exists('OutputFolder/'+args.outputDir):
    os.mkdir('OutputFolder/'+args.outputDir)
os.chdir('OutputFolder/'+args.outputDir)

# Recreate a root file to save the vector information of the '.csv' file
OutFile = TFile.Open(args.inputDir.split('/')[-1]+".root", "RECREATE")
MCP_tree = TTree("RAWData", "RAWData")
EventNumber = array('i',[0])
#debug_manchester_Vec = std.vector('long')([0])
decorate_data_Vec = std.vector('long')([0])
decorate_data_valid_Vec = std.vector('int')([0])
debug_decoded_hit_counter_Vec = std.vector('long')([0])
debug_start_counter_Vec = std.vector('long')([0])
debug_fsm_error_counter_Vec = std.vector('long')([0])
debug_generated_hit200_counter_Vec = std.vector('long')([0])
debug_generated_hit156_counter_r2_Vec = std.vector('long')([0])
TimeVec = std.vector('int')([0])
BC_ID_Vec = std.vector('int')([0])
Trigger_Vec = std.vector('int')([0])
Manchester_Vec = std.vector('int')([0])
MCP_tree.Branch("EventNumber",  EventNumber,  'EventNumber/I')
MCP_tree.Branch("TimeVec",  TimeVec)
MCP_tree.Branch("BC_ID_Vec",  BC_ID_Vec)
MCP_tree.Branch("Trigger_Vec",  Trigger_Vec)
MCP_tree.Branch("Manchester_Vec",  Manchester_Vec)
MCP_tree.Branch("decorate_data_Vec",  decorate_data_Vec)
MCP_tree.Branch("decorate_data_valid_Vec", decorate_data_valid_Vec)
MCP_tree.Branch("debug_decoded_hit_counter_Vec",  debug_decoded_hit_counter_Vec)
MCP_tree.Branch("debug_start_counter_Vec",  debug_start_counter_Vec)
MCP_tree.Branch("debug_fsm_error_counter_Vec",  debug_fsm_error_counter_Vec)
MCP_tree.Branch("debug_generated_hit200_counter_Vec",  debug_generated_hit200_counter_Vec)
MCP_tree.Branch("debug_generated_hit156_counter_r2_Vec",  debug_generated_hit156_counter_r2_Vec)

# enter the DataFolder and scan all the events of the test folder
EventList = os.listdir(Absolute_DirPath+args.inputDir)
NumberList = []
for i in range(len(EventList)):
    NumberList.append(int(EventList[i].split('.')[0]))  
NumberList.sort()
NewFileNameList = []
for i in range(len(NumberList)):
    NewFileNameList.append('{}'.format(NumberList[i])+'.csv')
for i in range(len(EventList)):
    if (i % 100) == 0:
        print("Event: ",i,len(EventList))
    # Set Varibale
    TimeList = []
    BC_ID_List = []
    TriggerList = []
    ManchesterList = []
    #debug_manchester_List = []
    decorate_data_List = []
    decorate_data_valid_List = []
    debug_decoded_hit_counter_list = []
    debug_start_counter_list = []
    debug_fsm_error_counter_list = []
    debug_generated_hit200_counter_list = []
    debug_generated_hit156_counter_r2_list = []
    
    TimeVec.clear()
    BC_ID_Vec.clear()
    Trigger_Vec.clear()
    Manchester_Vec.clear()
    #debug_manchester_Vec.clear()
    decorate_data_Vec.clear()
    decorate_data_valid_Vec.clear()
    debug_decoded_hit_counter_Vec.clear()
    debug_start_counter_Vec.clear()
    debug_fsm_error_counter_Vec.clear()
    debug_generated_hit200_counter_Vec.clear()
    debug_generated_hit156_counter_r2_Vec.clear()

    filename = Absolute_DirPath+args.inputDir + '/' +NewFileNameList[i]
    with open(filename) as f:
        f_csv = csv.reader(f)
        headers = next(f_csv)
        headers = next(f_csv)
        for row in f_csv:
            TimeList.append(int(row[0]))
            TriggerList.append(int(row[2]))
            BC_ID_List.append(row[3])
            ManchesterList.append(row[4])
            #debug_manchester_List.append(long(row[5]))
            decorate_data_List.append(int(row[6],16))
            decorate_data_valid_List.append(int(row[7]))
            debug_decoded_hit_counter_list.append(int(row[11],16))
            debug_start_counter_list.append(int(row[10],16))
            debug_fsm_error_counter_list.append(int(row[12],16))
            debug_generated_hit200_counter_list.append(int(row[13],16))
            debug_generated_hit156_counter_r2_list.append(int(row[14],16))
    
    EventNumber[0] = i
    for index in range(len(TimeList)):
        TimeVec.push_back(TimeList[index])
        Trigger_Vec.push_back(TriggerList[index])
        BC_ID_Vec.push_back(int(BC_ID_List[index],16))
        Manchester_Vec.push_back(int(ManchesterList[index],16))
        #debug_manchester_Vec.push_back(debug_manchester_List[index])
        decorate_data_Vec.push_back(decorate_data_List[index])
        decorate_data_valid_Vec.push_back(decorate_data_valid_List[index])
        debug_decoded_hit_counter_Vec.push_back(debug_decoded_hit_counter_list[index])
        debug_start_counter_Vec.push_back(debug_start_counter_list[index])
        debug_fsm_error_counter_Vec.push_back(debug_fsm_error_counter_list[index])
        debug_generated_hit200_counter_Vec.push_back(debug_generated_hit200_counter_list[index])
        debug_generated_hit156_counter_r2_Vec.push_back(debug_generated_hit156_counter_r2_list[index])
      
    MCP_tree.Fill()
 

OutFile.cd()
MCP_tree.Write()
OutFile.Close()    




