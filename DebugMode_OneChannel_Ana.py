from ROOT import *
from array import array
import os
import argparse

# Find the received signals by using debug_fsm_error_counter column
def GetDebugHitDir(tree1):
    Validation_value_List = list(tree1.decorate_data_valid_Vec)
    debug_decorated_List = list(tree1.debug_decoded_hit_counter_Vec)
    debug_fsm_error_List = list(tree1.debug_fsm_error_counter_Vec)
    Debug_Hit_Dir = {}
    Debug_Hit_Dir["index"] = []
    Debug_Hit_Dir["expected_hit_num"] = 0
    Debug_Hit_Dir["debug_decorated_judge"] = []
    Debug_Hit_Dir["Validation_judge"] = []
    for index in range(1,len(debug_fsm_error_List)):
        if debug_fsm_error_List[index] > debug_fsm_error_List[index-1]:
            Debug_Hit_Dir["expected_hit_num"] += 1
            Debug_Hit_Dir["index"].append(index)
    # Check the correctness/validity of the RawData
    for index in range(1,len(debug_fsm_error_List)):
        if index not in Debug_Hit_Dir["index"]:
            if debug_decorated_List[index] > debug_decorated_List[index-1] and Validation_value_List[index] == 0:
                print("======== Raw Data has some problems! Problem 1 =======")
    for i in range(len(Debug_Hit_Dir["index"])):
        Debug_Hit_Dir["debug_decorated_judge"].append(0)
        Debug_Hit_Dir["Validation_judge"].append(0)
    for i in range(len(Debug_Hit_Dir["index"])):
        if debug_decorated_List[Debug_Hit_Dir["index"][i]] > debug_decorated_List[Debug_Hit_Dir["index"][i]-1]:
            Debug_Hit_Dir["debug_decorated_judge"][i] = 1
        if Validation_value_List[Debug_Hit_Dir["index"][i]] > 0:
            Debug_Hit_Dir["Validation_judge"][i] = 1
    if  Debug_Hit_Dir["Validation_judge"] != Debug_Hit_Dir["debug_decorated_judge"]:
        print("======== Raw Data has some problems! Problem 2 =======")
    return Debug_Hit_Dir

# Calculate the decoding process time
def ProcessDataTimeCal(tree1,Debug_Hit_Dir):
    debug_start_list = list(tree1.debug_start_counter_Vec)
    debug_end_List = list(tree1.debug_fsm_error_counter_Vec)
    ProcessData_timeIndexNum_List = []
    for index in Debug_Hit_Dir["index"]:
        EndTime = debug_end_List[index]
        for j in range(50):
            if debug_start_list[index-j] == EndTime - 1 and debug_start_list[index-j+1] == EndTime:
                ProcessData_timeIndexNum_List.append(j-1)
                break
    return ProcessData_timeIndexNum_List

# Find the validated signal and calculate BC_Reco, LE, FE and ToT
def AcquireTimeInfo(tree1,Debug_Hit_Dir):
    TimeWidth = 0.23  # unit: ns
    Validated_Hit_List = []
    for i in range(len(Debug_Hit_Dir["Validation_judge"])):
        if Debug_Hit_Dir["Validation_judge"][i] == 1:
            Validated_Hit_List.append(Debug_Hit_Dir["index"][i])
    Data_TimeInfo_list = list(tree1.decorate_data_Vec)
    ValidatedSignal_TimeInfo_Dir = {}
    ValidatedSignal_TimeInfo_Dir["Number"] = 0
    ValidatedSignal_TimeInfo_Dir["Index"] = []
    ValidatedSignal_TimeInfo_Dir["RawData"] = []
    ValidatedSignal_TimeInfo_Dir["BC_ID_Reco"] = []
    ValidatedSignal_TimeInfo_Dir["LE"] = []
    ValidatedSignal_TimeInfo_Dir["FE"] = []
    ValidatedSignal_TimeInfo_Dir["ToT"] = []
    for index in Validated_Hit_List:
        ValidValue = bin(Data_TimeInfo_list[index]).split('b')[1]
        BinaryValue = '{:0>19d}'.format(int(ValidValue))
        BC_ID_Reco = int(BinaryValue[1:3],2)
        if BC_ID_Reco > 0:
            #print("========== BC_ID_Reco > 1, need to be processed separately! ==========")
            continue
        LeadingEdge = int(BinaryValue[3:11],2) * TimeWidth
        TrailingEdge = int(BinaryValue[11:19],2) * TimeWidth
        ToT = TrailingEdge - LeadingEdge
        ValidatedSignal_TimeInfo_Dir["Number"] += 1
        ValidatedSignal_TimeInfo_Dir["Index"].append(index)
        ValidatedSignal_TimeInfo_Dir["RawData"].append(BinaryValue)
        ValidatedSignal_TimeInfo_Dir["BC_ID_Reco"].append(BC_ID_Reco)
        ValidatedSignal_TimeInfo_Dir["LE"].append(LeadingEdge)
        ValidatedSignal_TimeInfo_Dir["FE"].append(TrailingEdge)
        ValidatedSignal_TimeInfo_Dir["ToT"].append(ToT)
    return ValidatedSignal_TimeInfo_Dir

# Enter the specific folder
parser = argparse.ArgumentParser(description="Read the DAQ data")
parser.add_argument("-i","--inputDir", required=True, help="Directory that contains input RootFile.")

args = parser.parse_args()

os.chdir('OutputFolder/'+args.inputDir)
DirPath = os.getcwd()

Signal_ValidDis = TH1F("Signal_ValidDis","",8,0,8)
TimeIndexHist = TH1F("TimeIndex_validated_Hist","",100,0,8200)
ExpectedTimeIndexHist = TH1F("TimeIndex_start_counter_Hit","",150,0,8500)
ExpectedHitNumHist = TH1F("CounterNumber_Hit","",10,0,10)
ProcessData_Time_hist = TH1F("Process_Time_hist_validated","",30,0,30)
ProcessData_Time_hist_all = TH1F("Process_Time_hist","",30,0,30)
LEDis = TH1F("LEDis","",40,0,40)
FEDis = TH1F("FEDis","",40,0,40)
ToTDis = TH1F("ToTDis","",40,0,40)

# Find the corresponding root file and start the analysis
RootFileList = os.listdir(DirPath)
for filename in RootFileList:
  if '.root' in filename:
    file1 = TFile.Open(filename)
    tree1 = file1.Get("RAWData")
    Signal_Valid_SumNum = 0
    for entry in range(tree1.GetEntries()):
        tree1.GetEntry(entry)
        if (entry % 500) == 0:
            print("Event: ",entry,tree1.GetEntries())
        # Find the received signals and save into a dictionary
        Debug_Hit_Dir = GetDebugHitDir(tree1)
        for index in Debug_Hit_Dir["index"]:
            ExpectedTimeIndexHist.Fill(index)
        ExpectedHitNumHist.Fill(Debug_Hit_Dir["expected_hit_num"])
        # Calculate the decoding process time
        ProcessData_timeIndexNum_List = ProcessDataTimeCal(tree1, Debug_Hit_Dir)
        # Find the validated signal and detailed time info
        ValidatedSignal_TimeInfo_Dir = AcquireTimeInfo(tree1,Debug_Hit_Dir)
        for i in range(len(ProcessData_timeIndexNum_List)):
            ProcessData_Time_hist_all.Fill(ProcessData_timeIndexNum_List[i])
            if Debug_Hit_Dir["Validation_judge"][i] == 1:
                ProcessData_Time_hist.Fill(ProcessData_timeIndexNum_List[i])
        ValidatedSignal_TimeInfo_list = AcquireTimeInfo(tree1,Debug_Hit_Dir)
        for i in range(ValidatedSignal_TimeInfo_list["Number"]):
            LEDis.Fill(ValidatedSignal_TimeInfo_list["LE"][i])
            FEDis.Fill(ValidatedSignal_TimeInfo_list["FE"][i])
            ToTDis.Fill(ValidatedSignal_TimeInfo_list["ToT"][i])
        Signal_ValidDis.Fill(ValidatedSignal_TimeInfo_list["Number"])
        for index in ValidatedSignal_TimeInfo_Dir["Index"]:
            TimeIndexHist.Fill(index)
    print("Signal_Valid_SumNum: ",Signal_Valid_SumNum)

    c3 = TCanvas("c3", "BPRE", 800, 600)
    c3.cd()
    c3.Divide(1,1,0.008,0.007)
    gPad.SetTopMargin(0.09)
    gPad.SetBottomMargin(0.10)
    gPad.SetLeftMargin(0.10)
    gPad.SetRightMargin(0.12)
    l = TLegend(0.70, 0.25, 0.88, 0.40)
    l.SetBorderSize(0)
    l.SetFillStyle(0)
    l.SetTextFont(42)
    l.SetTextSize(0.03)

    Signal_ValidDis.Draw("HE")
    Signal_ValidDis.SetLineColor(kBlue)
    Signal_ValidDis.SetLineWidth(2)
    ax = Signal_ValidDis.GetXaxis()
    ax.SetTitle( " Signal_Validation " )
    ay = Signal_ValidDis.GetYaxis()
    ay.SetTitle( " Event " )
    ay.SetTitleSize(0.04)
    ax.SetTitleOffset(1.0)
    ay.SetTitleOffset(1.2)
    ax.SetTitleSize(0.04)
    ax.SetLabelSize(0.04)
    ay.SetLabelSize(0.04)
    ax.Draw("same")
    ay.Draw("same")
    c3.SaveAs("Signal_Validation.png")

    TimeIndexHist.Draw("HE")
    TimeIndexHist.SetLineColor(kBlue)
    TimeIndexHist.SetLineWidth(2)
    ax = TimeIndexHist.GetXaxis()
    ax.SetTitle( " TimeIndex_validated_Hit " )
    ay = TimeIndexHist.GetYaxis()
    ay.SetTitle( " Event " )
    ay.SetTitleSize(0.04)
    ax.SetTitleOffset(1.0)
    ay.SetTitleOffset(1.2)
    ax.SetTitleSize(0.04)
    ax.SetLabelSize(0.04)
    ay.SetLabelSize(0.04)
    ax.Draw("same")
    ay.Draw("same")
    c3.SaveAs("Index_Hit.png")

    ExpectedTimeIndexHist.Draw("HE")
    ExpectedTimeIndexHist.SetLineColor(kBlue)
    ExpectedTimeIndexHist.SetLineWidth(2)
    ax = ExpectedTimeIndexHist.GetXaxis()
    ax.SetTitle( " TimeIndex_start_counter_Hit " )
    ay = ExpectedTimeIndexHist.GetYaxis()
    ay.SetTitle( " Event " )
    ay.SetTitleSize(0.04)
    ax.SetTitleOffset(1.0)
    ay.SetTitleOffset(1.2)
    ax.SetTitleSize(0.04)
    ax.SetLabelSize(0.04)
    ay.SetLabelSize(0.04)
    ax.Draw("same")
    ay.Draw("same")
    c3.SaveAs("Index_Hit_expected.png")

    ExpectedHitNumHist.Draw("HE")
    ExpectedHitNumHist.SetLineColor(kBlue)
    ExpectedHitNumHist.SetLineWidth(2)
    ax = ExpectedHitNumHist.GetXaxis()
    ax.SetTitle( " CounterNumber_Hit " )
    ay = ExpectedHitNumHist.GetYaxis()
    ay.SetTitle( " Event " )
    ay.SetTitleSize(0.04)
    ax.SetTitleOffset(1.0)
    ay.SetTitleOffset(1.2)
    ax.SetTitleSize(0.04)
    ax.SetLabelSize(0.04)
    ay.SetLabelSize(0.04)
    ax.Draw("same")
    ay.Draw("same")
    c3.SaveAs("Expected_Hit_Number.png")

    ProcessData_Time_hist.Draw("HE")
    ProcessData_Time_hist.SetLineColor(kBlue)
    ProcessData_Time_hist.SetLineWidth(2)
    ax = ProcessData_Time_hist.GetXaxis()
    ax.SetTitle( " Process_TimeIndexNum_Validated " )
    ay = ProcessData_Time_hist.GetYaxis()
    ay.SetTitle( " Event " )
    ay.SetTitleSize(0.04)
    ax.SetTitleOffset(1.0)
    ay.SetTitleOffset(1.2)
    ax.SetTitleSize(0.04)
    ax.SetLabelSize(0.04)
    ay.SetLabelSize(0.04)
    ax.Draw("same")
    ay.Draw("same")
    c3.SaveAs("ProcessData_TimeIndexNumber.png")

    ProcessData_Time_hist_all.Draw("HE")
    ProcessData_Time_hist_all.SetLineColor(kBlue)
    ProcessData_Time_hist_all.SetLineWidth(2)
    ax = ProcessData_Time_hist_all.GetXaxis()
    ax.SetTitle( " Process_TimeIndexNum " )
    ay = ProcessData_Time_hist_all.GetYaxis()
    ay.SetTitle( " Event " )
    ay.SetTitleSize(0.04)
    ax.SetTitleOffset(1.0)
    ay.SetTitleOffset(1.2)
    ax.SetTitleSize(0.04)
    ax.SetLabelSize(0.04)
    ay.SetLabelSize(0.04)
    ax.Draw("same")
    ay.Draw("same")
    c3.SaveAs("ProcessData_TimeIndexNumber_all.png")

    LEDis.Draw("HE")
    LEDis.SetLineColor(kBlue)
    LEDis.SetLineWidth(2)
    ax = LEDis.GetXaxis()
    ax.SetTitle( " LE (ns) " )
    ay = LEDis.GetYaxis()
    ay.SetTitle( " Event " )
    ay.SetTitleSize(0.04)
    ax.SetTitleOffset(1.0)
    ay.SetTitleOffset(1.2)
    ax.SetTitleSize(0.04)
    ax.SetLabelSize(0.04)
    ay.SetLabelSize(0.04)
    ax.Draw("same")
    ay.Draw("same")
    c3.SaveAs("LE.png")

    FEDis.Draw("HE")
    FEDis.SetLineColor(kBlue)
    FEDis.SetLineWidth(2)
    ax = FEDis.GetXaxis()
    ax.SetTitle( " FE (ns) " )
    ay = FEDis.GetYaxis()
    ay.SetTitle( " Event " )
    ay.SetTitleSize(0.04)
    ax.SetTitleOffset(1.0)
    ay.SetTitleOffset(1.2)
    ax.SetTitleSize(0.04)
    ax.SetLabelSize(0.04)
    ay.SetLabelSize(0.04)
    ax.Draw("same")
    ay.Draw("same")
    c3.SaveAs("FE.png")

    ToTDis.Draw("HE")
    ToTDis.SetLineColor(kBlue)
    ToTDis.SetLineWidth(2)
    ax = ToTDis.GetXaxis()
    ax.SetTitle( " ToTDis (ns) " )
    ay = ToTDis.GetYaxis()
    ay.SetTitle( " Event " )
    ay.SetTitleSize(0.04)
    ax.SetTitleOffset(1.0)
    ay.SetTitleOffset(1.2)
    ax.SetTitleSize(0.04)
    ax.SetLabelSize(0.04)
    ay.SetLabelSize(0.04)
    ax.Draw("same")
    ay.Draw("same")
    c3.SaveAs("ToTDis.png")