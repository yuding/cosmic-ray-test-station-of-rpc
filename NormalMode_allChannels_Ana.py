# -*- coding: utf-8 -*-
from ROOT import *
import csv
from array import array
import os
import argparse

# Find the Validation value and turn into the binary format and check the hit channel
def FindManchesterPoint(tree):
    IsManchesterList = list(tree.IsManchester_Vec)
    ManchesterValid_ValueList = []
    ManchesterTimeIndexList = []
    for index in range(len(IsManchesterList)-1):
        if IsManchesterList[index] > 0 and IsManchesterList[index-1] == 0 and IsManchesterList[index+1] == 0:
            ManchesterTimeIndexList.append(index)
            # Convert into binary value (corresponding to channel number)
            ValidValue = bin(IsManchesterList[index]).split('b')[1]
            ValidValue = '{:0>8d}'.format(int(ValidValue))
            ManchesterValid_ValueList.append(ValidValue)
    if len(ManchesterValid_ValueList) != len(ManchesterTimeIndexList):
        print(" ======= Read the ManchesterPoint wrongly! ========")
    return ManchesterTimeIndexList, ManchesterValid_ValueList

# Find the time information at the validation point 
def CatchManchesterPointValue(tree,ManchesterTimeIndexList, ManchesterValid_ValueList,ChannelNum):
    SelectedBinaryValueList = []
    IndexList = []
    for i in range(ChannelNum):
        SelectedBinaryValueList.append([])
    for i in range(len(ManchesterValid_ValueList)):
        if ManchesterValid_ValueList[i] != '00000001':
            continue
        num = ManchesterTimeIndexList[i]
        BinaryValueVecList = []
        BinaryValueVecList.append(list(tree.BinaryValue_Ch0_Vec)[num-80:num+80])
        BinaryValueVecList.append(list(tree.BinaryValue_Ch1_Vec)[num-80:num+80])
        BinaryValueVecList.append(list(tree.BinaryValue_Ch2_Vec)[num-80:num+80])
        BinaryValueVecList.append(list(tree.BinaryValue_Ch3_Vec)[num-80:num+80])
        BinaryValueVecList.append(list(tree.BinaryValue_Ch4_Vec)[num-80:num+80])
        BinaryValueVecList.append(list(tree.BinaryValue_Ch5_Vec)[num-80:num+80])
        BinaryValueVecList.append(list(tree.BinaryValue_Ch6_Vec)[num-80:num+80])
        BinaryValueVecList.append(list(tree.BinaryValue_Ch7_Vec)[num-80:num+80])
        for i in range(ChannelNum):
            start_index = -1
            ManchesterPointValue = -1
            for index in range(5,len(BinaryValueVecList[i])-5):
                if BinaryValueVecList[i][index-2] == 1 and BinaryValueVecList[i][index-1] == 1 and BinaryValueVecList[i][index] > 1 and BinaryValueVecList[i][index+1] > BinaryValueVecList[i][index] > 1:
                    start_index = index
                    break
            for index in range(start_index+2,len(BinaryValueVecList[i])-5):
                if BinaryValueVecList[i][index-1] > 10000 and BinaryValueVecList[i][index] > 10 and BinaryValueVecList[i][index+1] == 1 and BinaryValueVecList[i][index+2] == 1:
                    ManchesterPointValue = BinaryValueVecList[i][index]
                    SelectedBinaryValueList[i].append(ManchesterPointValue)
                    break
    return  SelectedBinaryValueList

# Use the time information to fill 19 bits and calculate BC_Reco, LE, FE, ToT
def TimeSplit(SelectedBinaryValueList,TimeWidth):
    ChannelNum = len(SelectedBinaryValueList)
    SignalNum = len(SelectedBinaryValueList[0])
    BC_ID_Reco_List = []
    LE_List = []
    FE_List = []
    LE_Bi_List = []
    FE_Bi_List = []
    ToT_List = []
    for i in range(ChannelNum):
        BC_ID_Reco_List.append([])
        LE_List.append([])
        FE_List.append([])
        LE_Bi_List.append([])
        FE_Bi_List.append([])
        ToT_List.append([])
    for ch in range(ChannelNum):
        for index in range(SignalNum):
            BinaryValue = '{:0>19d}'.format(SelectedBinaryValueList[ch][index])
            BC_ID_Reco = int(BinaryValue[1:3],2)
            LeadingEdge = int(BinaryValue[3:11],2) * TimeWidth
            TrailingEdge = int(BinaryValue[11:19],2) * TimeWidth
            LeadingEdge_Bi = int(BinaryValue[3:11],2)
            TrailingEdge_Bi = int(BinaryValue[11:19],2)
            ToT = TrailingEdge - LeadingEdge
            BC_ID_Reco_List[ch].append(BC_ID_Reco)
            LE_List[ch].append(LeadingEdge)
            FE_List[ch].append(TrailingEdge)
            ToT_List[ch].append(ToT)
            LE_Bi_List[ch].append(LeadingEdge_Bi)
            FE_Bi_List[ch].append(TrailingEdge_Bi)
    return BC_ID_Reco_List,LE_List,FE_List,ToT_List,LE_Bi_List,FE_Bi_List
            
        

# Enter the specific root file folder
parser = argparse.ArgumentParser(description="Read the DAQ data")
parser.add_argument("-i","--inputDir", required=True, help="Directory that contains input RootFile.")

args = parser.parse_args()

os.chdir('OutputFolder/'+args.inputDir)
DirPath = os.getcwd()

TimeWidth = 0.23  # unit: ns
ChannelNum = 1
LEDisList = []
FEDisList = []
ToTDisList = []
for i in range(ChannelNum):
    LEDisList.append(TH1F("LeadingEdgeDis_Ch{}".format(i),"",40,0,40))
    FEDisList.append(TH1F("TrailingEdgeDis_Ch{}".format(i),"",40,0,40))
    ToTDisList.append(TH1F("ToTDis_Ch{}".format(i),"",20,0,20))

TimeIndexHist = TH1F("TimeIndexHist","",100,0,8200)
Signal_ValidDis = TH1F("Signal_ValidDis","",5,0,5)

# Find the corresponding root file and start the analysis
RootFileList = os.listdir(DirPath)
for filename in RootFileList:
  if '.root' in filename:
    file1 = TFile.Open(filename)
    tree1 = file1.Get("RAWData")
    for entry in range(tree1.GetEntries()):
    #for entry in range(5):
        tree1.GetEntry(entry)
        if (entry % 500) == 0:
            print("Event: ",entry,tree1.GetEntries())    
        HitTimeIndexList, ManchesterValidList = FindManchesterPoint(tree1)
        Signal_Valid_Num = len(HitTimeIndexList)
        Signal_ValidDis.Fill(Signal_Valid_Num)
        for index in HitTimeIndexList:
            TimeIndexHist.Fill(index)
        # Only consider events with ManchesterValidation
        if len(ManchesterValidList) == 0:
            continue
        # Only consider events where Ch0 all have signals
        if '00000001' not in ManchesterValidList:
            continue
        BinaryValueList = CatchManchesterPointValue(tree1,HitTimeIndexList, ManchesterValidList,ChannelNum)
        SignalNum = len(BinaryValueList[0])
        BC_ID_Reco_List,LE_List,FE_List,ToT_List,LE_Bi_List,FE_Bi_List = TimeSplit(BinaryValueList,TimeWidth)
        for ch in range(ChannelNum):
            for index in range(SignalNum):
                ToTDisList[ch].Fill(ToT_List[ch][index])
                LEDisList[ch].Fill(LE_List[ch][index])
                FEDisList[ch].Fill(FE_List[ch][index])


    c3 = TCanvas("c3", "BPRE", 800, 600)
    c3.cd()
    c3.Divide(1,1,0.008,0.007)
    gPad.SetTopMargin(0.09)
    gPad.SetBottomMargin(0.10)
    gPad.SetLeftMargin(0.10)
    gPad.SetRightMargin(0.12)
    l = TLegend(0.70, 0.25, 0.88, 0.40)
    l.SetBorderSize(0)
    l.SetFillStyle(0)
    l.SetTextFont(42)
    l.SetTextSize(0.03)
    
    TimeIndexHist.Draw("HE")
    TimeIndexHist.SetLineColor(kBlue)
    TimeIndexHist.SetLineWidth(2)
    ax = TimeIndexHist.GetXaxis()
    ax.SetTitle( " TimeIndex_Hit " )
    ay = TimeIndexHist.GetYaxis()
    ay.SetTitle( " Event " )
    ay.SetTitleSize(0.04)
    ax.SetTitleOffset(1.0)
    ay.SetTitleOffset(1.2)
    ax.SetTitleSize(0.04)
    ax.SetLabelSize(0.04)
    ay.SetLabelSize(0.04)
    ax.Draw("same")
    ay.Draw("same")
    c3.SaveAs("Index_Hit.png")

    Signal_ValidDis.Draw("HE")
    Signal_ValidDis.SetLineColor(kBlue)
    Signal_ValidDis.SetLineWidth(2)
    ax = Signal_ValidDis.GetXaxis()
    ax.SetTitle( " Signal_Validation " )
    ay = Signal_ValidDis.GetYaxis()
    ay.SetTitle( " Event " )
    ay.SetTitleSize(0.04)
    ax.SetTitleOffset(1.0)
    ay.SetTitleOffset(1.2)
    ax.SetTitleSize(0.04)
    ax.SetLabelSize(0.04)
    ay.SetLabelSize(0.04)
    ax.Draw("same")
    ay.Draw("same")
    c3.SaveAs("Signal_Validation.png")

    for ch in range(ChannelNum):
        ToTDisList[ch].Draw("HE")
        ToTDisList[ch].SetLineColor(kBlue)
        ToTDisList[ch].SetLineWidth(2)
        ax = ToTDisList[ch].GetXaxis()
        ax.SetTitle( " ToT (ns) " )
        ay = ToTDisList[ch].GetYaxis()
        ay.SetTitle( " Event " )
        ay.SetTitleSize(0.04)
        ax.SetTitleOffset(1.0)
        ay.SetTitleOffset(1.2)
        ax.SetTitleSize(0.04)
        ax.SetLabelSize(0.04)
        ay.SetLabelSize(0.04)
        ax.Draw("same")
        ay.Draw("same")
        c3.SaveAs("ToT_Ch{}.png".format(ch))

        LEDisList[ch].Draw("HE")
        LEDisList[ch].SetLineColor(kBlue)
        LEDisList[ch].SetLineWidth(2)
        ax = LEDisList[ch].GetXaxis()
        ax.SetTitle( " LE (ns) " )
        ay = LEDisList[ch].GetYaxis()
        ay.SetTitle( " Event " )
        ay.SetTitleSize(0.04)
        ax.SetTitleOffset(1.0)
        ay.SetTitleOffset(1.2)
        ax.SetTitleSize(0.04)
        ax.SetLabelSize(0.04)
        ay.SetLabelSize(0.04)
        ax.Draw("same")
        ay.Draw("same")
        c3.SaveAs("LE_Ch{}.png".format(ch))

        FEDisList[ch].Draw("HE")
        FEDisList[ch].SetLineColor(kBlue)
        FEDisList[ch].SetLineWidth(2)
        ax = FEDisList[ch].GetXaxis()
        ax.SetTitle( " FE (ns) " )
        ay = FEDisList[ch].GetYaxis()
        ay.SetTitle( " Event " )
        ay.SetTitleSize(0.04)
        ax.SetTitleOffset(1.0)
        ay.SetTitleOffset(1.2)
        ax.SetTitleSize(0.04)
        ax.SetLabelSize(0.04)
        ay.SetLabelSize(0.04)
        ax.Draw("same")
        ay.Draw("same")
        c3.SaveAs("FE_Ch{}.png".format(ch))

    

