#ifndef HITRECONSTRUCTOR_H
#define HITRECONSTRUCTOR_H
#include <vector>
#include <QDebug>
#include <QObject>
#include <QMessageBox>
#include "fstream"

using namespace std;

#define LayerNum 3

struct PROCESSEDData
{
    int EventNo;
    int   NHits;
    vector<int>   *HitCh[LayerNum];
    vector<int>  *HitLEIndex[LayerNum];
    vector<int>  *HitFEIndex[LayerNum];
    vector<double>  *HitTime[LayerNum];
    vector<int>    *SignalCh[LayerNum];
    vector<int>  *SignalLEIndex[LayerNum];
    vector<int>  *SignalFEIndex[LayerNum];
    vector<double>  *SignalTime[LayerNum];   // 0 : Left-End  1： Right-End
};


class hitReconstructor : public QObject
{
    Q_OBJECT

private:
    uint                EventNumber;
    uint                TriggeredNumber;
    size_t              layerNO;
    int                 ByteStart;
    int                 ByteEnd;
    int                 left_LE_index;
    int                 left_FE_index;
    int                 right_LE_index;
    int                 right_FE_index;
    int                 signal_left_Ch;
    int                 signal_right_Ch;
    double              signal_left_LE;
    double              signal_left_FE;
    double              signal_right_LE;
    double              signal_right_FE;
    int                 signal_left_ToT;       // fastest ToT
    int                 signal_right_ToT;
    double              RefTime;
    double              temp_time;
    int                 temp_right_index;
    int                 temp_left_index;
    int                 temp_ref_index;


    vector<int>         *temp_LECh, *SC_LECh;
    vector<float>       *temp_LE, *SC_LE;
    vector<int>         *temp_FECh, *SC_FECh;
    vector<float>       *temp_FE, *SC_FE;

    vector<bool>        *temp_PhiPaired;
    vector<bool>        *temp_EtaPaired;
    std::ofstream outFile;

public:
    explicit            hitReconstructor(QObject *parent = nullptr);
    void                mapinit();
    void                clear();
    void                process(int startbyte, int endbyte, double reftime, std::vector<int> channelHitVec, std::vector<int> LEFEDisVec, std::vector<double> TimeVec);
    void                reconstruct();
    void                calculate();
    void                tracking();
    void                temp_clear();
    float               SumOfProduct(std::vector<float> Var1, std::vector<float> Var2);
    float               SumOfProduct(std::vector<float> Var1, float C = 1);
    PROCESSEDData       processedData;

};

#endif // HITRECONSTRUCTOR_H
