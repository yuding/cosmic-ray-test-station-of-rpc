#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include "qcustomplot.h"
#include "hitreconstructor.h"
#include "histogram.h"

#include <QMainWindow>
#include <QProcess>
#include <QMessageBox>
#include <QTimer>
#include <QFileDialog>
#include <QFile>
#include <QFileInfo>
#include <QDataStream>
#include <QTextStream>
#include <QDebug>
#include <vector>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    hitReconstructor *hitRecon;
    histogram *Right_LE_hist;
    histogram *Left_LE_hist;
    histogram *hitCh_hist;
    histogram *Eff_Or_hist;

    bool isNum(QString str);
    void InfoProcess();
    void EventReconstruction();

private slots:

    void on_startButtom_clicked();
    void SingleEventReco();

    void on_endlButtom_clicked();
    void on_pushButton_clicked();
    void on_FileOpenButton_clicked();


private:
    Ui::MainWindow *ui;
    QTimer *timer;
    int TimerCount;
    QCustomPlot *Plot_RightLE;
    QCustomPlot *Plot_LeftLE;
    QCustomPlot *Plot_HitCh;
    QCustomPlot *Plot_Eff_Or;
    QString OpenFileName;
    QString interval_read;
    QFile file;
    int fileLen;
    int HitNum;
    int EventNum;
    int EventCount;
    int ishift;
    double cycle;
    char *s;

    // Used for the hit reconstruction
    int StartByte;
    int EndByte;
    double RefTime;

    // Decoded contents
    std::vector<quint8> byteVec;
    std::vector<int> channelHitVec;
    std::vector<int> EventNumVec;
    std::vector<int> LEFEDisVec;
    std::vector<double> RoughCountVec;
    std::vector<double> FineCountVec;
    std::vector<double> TimeVec;
    std::vector<int> startVec;

    // Event reconstruction
    std::vector<int> ByteStartVec;     // start byte for each event
    std::vector<int> ByteEndVec;     // end byte for each event
    std::vector<double> RefTimeVec;  //  refTime for each event

};
#endif // MAINWINDOW_H
