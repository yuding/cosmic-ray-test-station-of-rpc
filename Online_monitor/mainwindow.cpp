#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    timer = new QTimer;
    TimerCount = 0;
    ui->cmdLineEdit->setText(QString::number(TimerCount));

    Plot_RightLE = ui->plot1;
    Plot_LeftLE = ui->plot_LE_2;
    Plot_HitCh = ui->plot2;
    Plot_Eff_Or = ui->plot_eff;
    Right_LE_hist = new histogram(Right_LE, Plot_RightLE, this);
    Left_LE_hist = new histogram(Left_LE, Plot_LeftLE, this);
    hitCh_hist = new histogram(All_HitCh, Plot_HitCh, this);
    Eff_Or_hist = new histogram(All_EFF, Plot_Eff_Or, this);

    Right_LE_hist->init(370, 440, 2, Right);
    Left_LE_hist->init(370, 440, 2, Left);
    hitCh_hist->init(1, 32, 1, All);
    Eff_Or_hist->init(100, 500, 1, All);

    connect(ui->FileOpenButton, SIGNAL(clicked()), this, SLOT(on_FileOpenButton_clicked()));

    hitRecon = new hitReconstructor();
    connect(timer, SIGNAL(timeout()), this, SLOT(SingleEventReco()));

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_startButtom_clicked()
{
    interval_read = ui->intervalEdit->text();
    if(interval_read.isEmpty())
    {
        timer->start(1000);
    }else if(isNum(interval_read))
    {
        qDebug() << "Have input the Interval! ";
        timer->start(1000 * interval_read.toDouble());
    }else
    {
        QMessageBox::warning(this, "Warning", "Please input the true interval!");
    }
}

bool MainWindow::isNum(QString str)
{
    bool isNum;
    str.toDouble(&isNum);
    return isNum;
}

void MainWindow::on_endlButtom_clicked()
{
    timer->stop();
    TimerCount = 0;
    ui->cmdLineEdit->setText(QString::number(TimerCount));
}

void MainWindow::on_pushButton_clicked()
{
    timer->stop();
}


void MainWindow::on_FileOpenButton_clicked()
{
    OpenFileName = QFileDialog::getOpenFileName(this, "Choose one file", QCoreApplication::applicationFilePath(), "*.bin");
    if (OpenFileName.isEmpty())
    {
       QMessageBox::warning(this, "Warning", "Please choose one file!");
    } else {
        ui->FileInputEdit->setText(OpenFileName);
        QFile file(OpenFileName);
        //file.open(QIODevice::ReadOnly);
        if(!file.open(QIODevice::ReadOnly))
        {
             qCritical() << "Failed to open file";
             return;
        };
        QFileInfo fileInfo(OpenFileName);
        // fileLen: the number of bytes (8 bytes -> 1 hit; 8 bits -> 1 byte)
        fileLen = fileInfo.size();
        QDataStream in(&file);
        byteVec.resize(fileLen, 0);
        for(int i = 0; i < byteVec.size(); i++)
        {
            in >> byteVec[i];
        }
        QMessageBox::information(this, "Info", "Data have been processed successfully");

        InfoProcess();
        EventReconstruction();
        file.close();
    }
}

void MainWindow::InfoProcess()
{
    HitNum = floor(byteVec.size() / 8);
    qDebug() << "Number of events: " << HitNum;

    //通道号 事例编号 前后沿 粗技术 细计数 start标志
    channelHitVec.resize(HitNum, 0);
    EventNumVec.resize(HitNum, 0);
    LEFEDisVec.resize(HitNum, 0);
    RoughCountVec.resize(HitNum, 0.);
    FineCountVec.resize(HitNum, 0.);
    TimeVec.resize(HitNum, 0.);
    startVec.resize(HitNum, 0);

    cycle = 2.5;  //粗计数时间周期
    ishift = 0;
    EventNum = 0;
    // Data Process
    for(int i=0; i<HitNum; i++)
    {
        channelHitVec[i] = byteVec[i*8+ishift+1] % (int)pow(2,7);
        EventNumVec[i] = (byteVec[i*8+ishift+2]-128) * (int)pow(2,7) + byteVec[i*8+ishift+3];
        LEFEDisVec[i] = floor(byteVec[i*8+ishift+7]/32);   // 2--LE 3--FE
        RoughCountVec[i] = (byteVec[i*8+ishift+4]-128) * (int)pow(2,14) + byteVec[i*8+ishift+5] * (int)pow(2,7) + (byteVec[i*8+ishift+6]-128);
        FineCountVec[i] = byteVec[i*8+ishift+7] % 32;
        TimeVec[i] = RoughCountVec[i] * 2.5 + FineCountVec[i] * (double)(2.5/24);  // Unit: ns
        startVec[i] = floor(byteVec[i*8+ishift] / 32);

        if(startVec[i] != 7)
        {
            channelHitVec[i] += 1;
        } else
        {
            EventNum += 1;
        }
    }
    qDebug() << "Number of events: " << EventNum;
}

void MainWindow::EventReconstruction()
{
    //Event Reconstruction -- EventByEvent
    ByteStartVec.resize(EventNum-1, 0);
    ByteEndVec.resize(EventNum-1, 0);
    RefTimeVec.resize(EventNum-1, 0);
    EventCount = 0;
    for(int i=0; i<HitNum; i++)
    {
        if(startVec[i] == 7)
        {
            if (EventCount == 0)
            {
               ByteStartVec[EventCount] = i+1;
            }else if(EventCount == EventNum - 1)
            {
                ByteEndVec[EventCount-1] = i-1;
                break;
            }else
            {
                ByteEndVec[EventCount-1] = i-1;
                ByteStartVec[EventCount] = i+1;
            }
            RefTimeVec[EventCount] = TimeVec[i] - 500;
            EventCount += 1;
        }

    }
}

void MainWindow::SingleEventReco()
{
    qDebug() << "In the event " << TimerCount;
    StartByte = ByteStartVec[TimerCount];
    EndByte = ByteEndVec[TimerCount];
    RefTime = RefTimeVec[TimerCount];
    hitRecon->clear();
    hitRecon->process(StartByte, EndByte, RefTime, channelHitVec, LEFEDisVec, TimeVec);
    Right_LE_hist->refreshLE(hitRecon->processedData);
    Left_LE_hist->refreshLE(hitRecon->processedData);
    hitCh_hist->refreshCH(hitRecon->processedData);
    Eff_Or_hist->refreshEff(hitRecon->processedData, TimerCount);
    TimerCount ++;
    ui->cmdLineEdit->setText(QString::number(TimerCount));

    if(TimerCount == 250)
    {
        timer->stop();
        QString EncodedText1 = "All the ";
        QString EncodedText2 = " events have been encoded and plotted！";
        QMessageBox::information(this, "Info", EncodedText1+QString::number(TimerCount)+EncodedText2);
    }
}


