﻿#include "histogram.h"

histogram::histogram(int type, QCustomPlot *customPlot0, QWidget *parent):
    QWidget(parent)
{
    customPlot = customPlot0;
    binSettedFlag = false;
    totalHits = 0;
    signalHits = 0;
    TYPE = type;
    totalEventNumber = 0;
    triggeredEventNumber = 0;
    EffBinNO = 120;
    EffShowNO = 100;
}
void histogram::init(double st, double ed, double binwidth, size_t layerno)
{
    layerNO = layerno;
    binGap = binwidth;
    startPoint = st;
    endPoint = ed;
    binNumber = (endPoint - startPoint)/binGap + 1;
    x1.resize(binNumber);y1.resize(binNumber);
    x2.resize(binNumber);y2.resize(binNumber);
    y3.resize(binNumber);  //For Channel Ref Efficiency
    for (int i = 0; i < binNumber; i ++)
    {
        x1[i] = startPoint + binGap * i;
        x2[i] = startPoint + binGap * i;
        y1[i] = 0;
        y2[i] = 0;
    }
    endPoint += binGap/2.0;
    startPoint -= binGap/2.0;

    // initial title
    customPlot->plotLayout()->insertRow(0);
    title = new QCPTextElement(customPlot, " ");
    customPlot->plotLayout()->addElement(0, 0, title);
    //title->setAutoMargins(QCP::msNone);
    //title->setMargins(QMargins(0,0,0,0));
    customPlot->xAxis->setLabelPadding(0);
    customPlot->xAxis->setTickLabelPadding(0);
    customPlot->xAxis->setRange(startPoint, endPoint);

    // create and configure plottables:
    bars1 = new QCPBars(customPlot->xAxis, customPlot->yAxis);
    bars1->setWidth(binGap);
    bars1->setData(x1, y1);
    bars1->setPen(Qt::NoPen);
    bars1->setBrush(QColor(10, 140, 70, 160));

    if(TYPE == Right_LE || TYPE == Left_LE)
     {
        if(TYPE == Right_LE)
        {
            customPlot->plotLayout()->insertRow(0);
            QCPTextElement *title = new QCPTextElement(customPlot, "Right_End", QFont("sans", 12, QFont::Bold));
            customPlot->plotLayout()->addElement(0, 0, title);
        }else{
            customPlot->plotLayout()->insertRow(0);
            QCPTextElement *title = new QCPTextElement(customPlot, "Left_End", QFont("sans", 12, QFont::Bold));
            customPlot->plotLayout()->addElement(0, 0, title);
        }
        customPlot->xAxis->setLabel("Leading edge (ns)");
        customPlot->yAxis->setLabel("Event Number");
     }
     if(TYPE == All_HitCh)
     {
        customPlot->plotLayout()->insertRow(0);
        QCPTextElement *title = new QCPTextElement(customPlot, "HitChannel", QFont("sans", 12, QFont::Bold));
        customPlot->plotLayout()->addElement(0, 0, title);
        customPlot->xAxis->setLabel("Hit Channel");
        customPlot->yAxis->setLabel("Event Number");
     }

    if(TYPE == Right_TOT || TYPE == Left_TOT)
    {
        graph1 = customPlot->addGraph();
        graph1->setData(x2, y2);
        graph1->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCross, QPen(Qt::black, 1.5), QBrush(Qt::white), 9));
        graph1->setPen(QPen(QColor(120, 120, 120), 2));
    }


    // move bars above graphs and grid below bars:
    customPlot->addLayer("abovemain", customPlot->layer("main"), QCustomPlot::limAbove);
    customPlot->addLayer("belowmain", customPlot->layer("main"), QCustomPlot::limBelow);
    if(TYPE == Right_EFF || TYPE == Left_EFF || TYPE == All_EFF)
    {
        customPlot->plotLayout()->insertRow(0);
        QCPTextElement *title = new QCPTextElement(customPlot, "Efficiency", QFont("sans", 12, QFont::Bold));
        customPlot->plotLayout()->addElement(0, 0, title);
        customPlot->xAxis->setLabel("Event Number");
        customPlot->yAxis->setLabel("Trigger Efficiency [%]");
        //customPlot->yAxis->setRangeLower(0.0);
        //customPlot->yAxis->setRangeUpper(100.0);
        customPlot->yAxis->setRange(0.0, 100.0);
    }
    // set some pens, brushes and backgrounds:
    customPlot->xAxis->grid()->setPen(QPen(QColor(140, 140, 140), 1, Qt::DotLine));
    customPlot->yAxis->grid()->setPen(QPen(QColor(140, 140, 140), 1, Qt::DotLine));
    customPlot->xAxis->grid()->setSubGridPen(QPen(QColor(80, 80, 80), 1, Qt::DotLine));
    customPlot->yAxis->grid()->setSubGridPen(QPen(QColor(80, 80, 80), 1, Qt::DotLine));
    customPlot->xAxis->grid()->setSubGridVisible(true);
    customPlot->yAxis->grid()->setSubGridVisible(true);
    customPlot->replot();

    binSettedFlag = true;
}

void histogram::refreshLE(PROCESSEDData &Data)
{
    if (binSettedFlag)
    {
        if (Data.HitLEIndex[layerNO]->size() > 0)
        {
            for(int i = 0;i<Data.HitLEIndex[layerNO]->size();i++)
            {
                temp = double(Data.HitTime[layerNO]->at(Data.HitLEIndex[layerNO]->at(i)));
                if(temp >= startPoint && temp < endPoint)
                {
                    int No = int((temp - startPoint)/binGap);
                    y1[No] += 1.0;
                }
            }
        }
        bars1->setData(x1, y1);
        customPlot->rescaleAxes();
        customPlot->replot(QCustomPlot::rpQueuedReplot);
    }
    else
        qDebug() << "Bins haven't been set";
}

void histogram::refreshCH(PROCESSEDData &Data)
{
    if (binSettedFlag)
    {
        if(Data.HitCh[1]->size() > 0)
        {
            for(int i = 0;i<Data.HitCh[1]->size();i++)
            {
                int No = int((Data.HitCh[1]->at(i) - startPoint)/binGap);
                y1[No] += 1.0;
            }
        }
        if(Data.HitCh[0]->size() > 0)
        {
            for(int i = 0;i<Data.HitCh[0]->size();i++)
            {
                int No = int((Data.HitCh[0]->at(i) - startPoint)/binGap);
                y1[No] += 1.0;
            }
        }
        if(Data.HitCh[2]->size() > 0)
        {
            for(int i = 0;i<Data.HitCh[2]->size();i++)
            {
                int No = int((Data.HitCh[2]->at(i) - startPoint)/binGap);
                y1[No] += 1.0;
            }
        }

        bars1->setData(x1, y1);
        customPlot->rescaleAxes();
        customPlot->replot(QCustomPlot::rpQueuedReplot);
    }
    else
        qDebug() << "Bins haven't been set";
}

void histogram::refreshEff(PROCESSEDData &Data, int EventNo)
{
    if (binSettedFlag)
    {
        if (Data.HitLEIndex[2]->size() > 0)
        {
            totalHits += 1;
            if (Data.HitLEIndex[0]->size() > 0 || Data.HitLEIndex[1]->size() > 0)
            {
                signalHits += 1;
            }
        }
        if(EventNo > startPoint+1)
        {
            int No = int((EventNo - startPoint)/binGap);
            y1[No] += (double(signalHits) / totalHits)*100;
            bars1->setData(x1, y1);
            //customPlot->rescaleAxes();
            customPlot->replot(QCustomPlot::rpQueuedReplot);
        }
    }
    else
        qDebug() << "Bins haven't been set";
}

