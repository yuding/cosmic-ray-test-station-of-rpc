#include "hitreconstructor.h"

using namespace std;
hitReconstructor::hitReconstructor(QObject *parent) : QObject(parent)
{
    processedData.EventNo = 0;
    processedData.NHits = 0;
    for(int i = 0; i < LayerNum; i++)
    {
        processedData.HitCh[i] = new vector<int>;       // 0: Right_End  1: Left_End
        processedData.HitLEIndex[i] = new vector<int>;
        processedData.HitFEIndex[i] = new vector<int>;
        processedData.HitTime[i] = new vector<double>;
        processedData.SignalCh[i] = new vector<int>;
        processedData.SignalLEIndex[i] = new vector<int>;
        processedData.SignalFEIndex[i] = new vector<int>;
        processedData.SignalTime[i] = new vector<double>;
    }
}

void hitReconstructor::clear()
{
    for(int i = 0; i < LayerNum; i++)
    {
       delete processedData.HitCh[i];
       delete processedData.HitLEIndex[i];
       delete processedData.HitFEIndex[i];
       delete processedData.HitTime[i];
       delete processedData.SignalCh[i];
       delete processedData.SignalLEIndex[i];
       delete processedData.SignalFEIndex[i];
       delete processedData.SignalTime[i];
    }

    for(int i = 0; i < LayerNum; i++)
    {
        processedData.HitCh[i] = new vector<int>;
        processedData.HitLEIndex[i] = new vector<int>;
        processedData.HitFEIndex[i] = new vector<int>;
        processedData.HitTime[i] = new vector<double>;
        processedData.SignalCh[i] = new vector<int>;
        processedData.SignalLEIndex[i] = new vector<int>;
        processedData.SignalFEIndex[i] = new vector<int>;
        processedData.SignalTime[i] = new vector<double>;
    }
}


void hitReconstructor::process(int startbyte, int endbyte, double reftime, std::vector<int> channelHitVec, std::vector<int> LEFEDisVec, std::vector<double> TimeVec)
{
    ByteStart = startbyte;
    ByteEnd = endbyte;
    RefTime = reftime;
    temp_right_index = 0;
    temp_left_index = 0;
    temp_ref_index = 0;
    for(int i=ByteStart;i<ByteEnd+1;i++)
    {
        temp_time = 0.;
        temp_time = TimeVec[i]-RefTime;
        if(channelHitVec[i] < 7 && temp_time > 380 && temp_time < 440)
        {
           processedData.HitCh[1]->push_back(channelHitVec[i]);
           if(LEFEDisVec[i]==2)
           {
               processedData.HitLEIndex[1]->push_back(temp_right_index);
               processedData.HitTime[1]->push_back(temp_time);
           }else if(LEFEDisVec[i]==3)
           {
               processedData.HitFEIndex[1]->push_back(temp_right_index);
               processedData.HitTime[1]->push_back(temp_time);
           }else
           {
               qDebug() << "WARNING! Leading edge and Trailing edge are loading wrongly!";
               break;
           }
           temp_right_index += 1;
        }

        if(channelHitVec[i] > 6 && channelHitVec[i] < 13 && temp_time > 380 && temp_time < 440)
        {
           processedData.HitCh[0]->push_back(channelHitVec[i]);
           if(LEFEDisVec[i]==2)
           {
               processedData.HitLEIndex[0]->push_back(temp_left_index);
               processedData.HitTime[0]->push_back(temp_time);
           }else if(LEFEDisVec[i]==3)
           {
               processedData.HitFEIndex[0]->push_back(temp_left_index);
               processedData.HitTime[0]->push_back(temp_time);
           }else
           {
               qDebug() << "WARNING! Leading edge and Trailing edge are loading wrongly!";
               break;
           }
           temp_left_index += 1;
        }

        if(channelHitVec[i] > 15 && channelHitVec[i] < 33 && temp_time > 380 && temp_time < 440)
        {
           processedData.HitCh[2]->push_back(channelHitVec[i]);
           if(LEFEDisVec[i]==2)
           {
               processedData.HitLEIndex[2]->push_back(temp_ref_index);
               processedData.HitTime[2]->push_back(temp_time);
           }else if(LEFEDisVec[i]==3)
           {
               processedData.HitFEIndex[2]->push_back(temp_ref_index);
               processedData.HitTime[2]->push_back(temp_time);
           }else
           {
               qDebug() << "WARNING! Leading edge and Trailing edge are loading wrongly!";
               break;
           }
           temp_ref_index += 1;
        }
    }
    //reconstruct();
}

void hitReconstructor::reconstruct()
{
    // Left Signal reconstruction
    left_LE_index = -1;
    left_FE_index = -1;
    right_LE_index = -1;
    right_FE_index = -1;
    signal_left_Ch = -1;
    signal_right_Ch = -1;
    signal_left_LE = 10000.;
    signal_left_FE = 10000.;
    signal_right_LE = 10000.;
    signal_right_FE = 10000.;
    signal_left_ToT = -1.;
    signal_right_ToT = -1.;

    for(int i=0;i<processedData.HitLEIndex[0]->size();i++)
    {
        if(processedData.HitTime[0]->at(processedData.HitLEIndex[0]->at(i)) < signal_left_LE)
        {
           signal_left_LE = processedData.HitTime[0]->at(i);
           left_LE_index = i;
        }
    }
    if(left_LE_index > -1)
    {
        for(int i=0;i<processedData.HitFEIndex[0]->size();i++)
        {
            if(processedData.HitTime[0]->at(processedData.HitFEIndex[0]->at(i)) < signal_left_FE && processedData.HitCh[0]->at(processedData.HitFEIndex[0]->at(i)) == processedData.HitCh[0]->at(left_LE_index))
            {
                signal_left_FE = processedData.HitTime[0]->at(i);
                left_FE_index = i;
            }
        }
    }
    if(left_LE_index > -1 && left_FE_index > -1)
    {
        signal_left_ToT = signal_left_FE - signal_left_LE;
        signal_left_Ch = processedData.HitCh[0]->at(left_LE_index);
    }
}
