#ifndef LEADINGEDGE_H
#define LEADINGEDGE_H
#include "qcustomplot.h"
#include "hitreconstructor.h"
#include "QVector"
#include <vector>
#include <sstream>
#define Left 0
#define Right 1
#define Ref   2
#define All   3
#define Right_CH 0
#define Right_LE 1
#define Right_FE 2
#define Right_CS 3
#define Right_TOT 4
#define Right_EFF 5
#define Left_CH 6
#define Left_LE 7
#define Left_FE 8
#define Left_CS 9
#define Left_TOT 10
#define Left_EFF 11
#define All_EFF 12
#define All_HitCh 13
#define Ref_LE 14
#define TYPE_LEFT false
#define TYPE_RIGHT true
class histogram : public QWidget
{
    Q_OBJECT

public:
    histogram(int type, QCustomPlot *customPlot0, QWidget *parent = nullptr);
    void init(double st, double ed, double binwidth, size_t layerno, bool PhiorEta);
    void init(double st, double ed, double binwidth, size_t layerno, bool PhiorEta, size_t totallayerno);
    void init(double st, double ed, double binwidth, size_t layerno); //For Eff only
    void refreshLE(PROCESSEDData &Data);
    void refreshCH(PROCESSEDData &Data);
    void refreshEff(PROCESSEDData &Data, int EventNo);

    void clearDisplay();

private:
    QCustomPlot *customPlot;
    QVector<double> x1, y1;
    QVector<double> x2, y2, y3;
    QVector<QVector<double>> stat;
    QVector<double> error;
    double y1Error = 0;
    QCPGraph *graph1;
    QCPBars *bars1;
    QCPTextElement* title;
    std::stringstream titleText;
    double startPoint;
    double endPoint;
    int binNumber;
    double binGap;
    bool binSettedFlag;
    int totalHits;
    int signalHits;
    size_t layerNO;
    size_t totalLayerNO;
    double temp;

    bool PHIorETA;
    int TYPE;
    int totalEventNumber;
    int triggeredEventNumber;
    int EffBinNO;
    int EffShowNO;
    QCPErrorBars *errorBars = nullptr;
signals:
    void sendReplot(uint l, bool phioreta);
    void sendReplot(uint l);
};

#endif // LEADINGEDGE_H
