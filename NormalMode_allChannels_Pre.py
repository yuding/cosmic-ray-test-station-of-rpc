# -*- coding: utf-8 -*-
from ROOT import *
import csv
from array import array
import os
import argparse

# Use argparse to set the configuration
parser = argparse.ArgumentParser(description="Read the DAQ data")
parser.add_argument("-i","--inputDir", required=True, help="Directory that contains input files to loop over.")
parser.add_argument("-o","--outputDir", required=True, help="Directory that save the Rootfile.")

args = parser.parse_args() 

DirPath = os.getcwd()
Absolute_DirPath = os.path.abspath(DirPath) + '/'

# Set OutputFolder and enter the folder
if not os.path.exists('OutputFolder/'+args.outputDir):
    os.mkdir('OutputFolder/'+args.outputDir)
os.chdir('OutputFolder/'+args.outputDir)

# Recreate a root file to save the vector information of the '.csv' file
OutFile = TFile.Open(args.inputDir.split('/')[-1]+".root", "RECREATE")
MCP_tree = TTree("RAWData", "RAWData")
EventNumber = array('i',[0])
IsGoodEvent = array('i',[0])    # All channels have signals
IsDiscrimatorOR = array('i',[0])
TimeIndex = array('i',[0])
BC_ID_value = array('i',[0])
Manchester_Ch0 = array('l',[0])
Manchester_Ch1 = array('l',[0])
Manchester_Ch2 = array('l',[0])
Manchester_Ch3 = array('l',[0])
Manchester_Ch4 = array('l',[0])
Manchester_Ch5 = array('l',[0])
Manchester_Ch6 = array('l',[0])
Manchester_Ch7 = array('l',[0])
Manchester_Vec_Ch0 = std.vector('long')([0])
Manchester_Vec_Ch1 = std.vector('long')([0])
Manchester_Vec_Ch2 = std.vector('long')([0])
Manchester_Vec_Ch3 = std.vector('long')([0])
Manchester_Vec_Ch4 = std.vector('long')([0])
Manchester_Vec_Ch5 = std.vector('long')([0])
Manchester_Vec_Ch6 = std.vector('long')([0])
Manchester_Vec_Ch7 = std.vector('long')([0])
TimeVec = std.vector('int')([0])
BC_ID_Vec = std.vector('int')([0])
IsManchester_Vec = std.vector('int')([0])
IsDiscrimatorOR_Vec = std.vector('int')([0])
MCP_tree.Branch("EventNumber",  EventNumber,  'EventNumber/I')
MCP_tree.Branch("IsGoodEvent",  IsGoodEvent,  'IsGoodEvent/I')
MCP_tree.Branch("IsDiscrimatorOR",  IsDiscrimatorOR,  'IsDiscrimatorOR/I')
MCP_tree.Branch("TimeIndex_Hit",  TimeIndex,  'TimeIndex/I')
MCP_tree.Branch("BC_ID",  BC_ID_value,  'BC_ID/I')
MCP_tree.Branch("BinaryValue_Ch0_Hit",  Manchester_Ch0,  'BinaryValue_Ch0_Hit/L')
MCP_tree.Branch("BinaryValue_Ch1_Hit",  Manchester_Ch1,  'BinaryValue_Ch1_Hit/L')
MCP_tree.Branch("BinaryValue_Ch2_Hit",  Manchester_Ch2,  'BinaryValue_Ch2_Hit/L')
MCP_tree.Branch("BinaryValue_Ch3_Hit",  Manchester_Ch3,  'BinaryValue_Ch3_Hit/L')
MCP_tree.Branch("BinaryValue_Ch4_Hit",  Manchester_Ch4,  'BinaryValue_Ch4_Hit/L')
MCP_tree.Branch("BinaryValue_Ch5_Hit",  Manchester_Ch5,  'BinaryValue_Ch5_Hit/L')
MCP_tree.Branch("BinaryValue_Ch6_Hit",  Manchester_Ch6,  'BinaryValue_Ch6_Hit/L')
MCP_tree.Branch("BinaryValue_Ch7_Hit",  Manchester_Ch7,  'BinaryValue_Ch7_Hit/L')
MCP_tree.Branch("IsManchester_Vec",  IsManchester_Vec)
MCP_tree.Branch("IsDiscrimatorOR_Vec",  IsDiscrimatorOR_Vec)
MCP_tree.Branch("TimeVec",  TimeVec)
MCP_tree.Branch("BC_ID_Vec",  BC_ID_Vec)
MCP_tree.Branch("BinaryValue_Ch0_Vec",  Manchester_Vec_Ch0)
MCP_tree.Branch("BinaryValue_Ch1_Vec",  Manchester_Vec_Ch1)
MCP_tree.Branch("BinaryValue_Ch2_Vec",  Manchester_Vec_Ch2)
MCP_tree.Branch("BinaryValue_Ch3_Vec",  Manchester_Vec_Ch3)
MCP_tree.Branch("BinaryValue_Ch4_Vec",  Manchester_Vec_Ch4)
MCP_tree.Branch("BinaryValue_Ch5_Vec",  Manchester_Vec_Ch5)
MCP_tree.Branch("BinaryValue_Ch6_Vec",  Manchester_Vec_Ch6)
MCP_tree.Branch("BinaryValue_Ch7_Vec",  Manchester_Vec_Ch7)

# enter the DataFolder and scan all the events of the test folder
EventList = os.listdir(Absolute_DirPath+args.inputDir)
NumberList = []
for i in range(len(EventList)):
    NumberList.append(int(EventList[i].split('.')[0]))  
NumberList.sort()
NewFileNameList = []
for i in range(len(NumberList)):
    NewFileNameList.append('{}'.format(NumberList[i])+'.csv')
for i in range(len(EventList)):
    if (i % 100) == 0:
        print("Event: ",i,len(EventList))
    # Set Varibale
    TimeList = []
    IsManchesterList = []
    IsDiscrimatorORList = []
    ManchesterList_Ch0 = []
    ManchesterList_Ch1 = []
    ManchesterList_Ch2 = []
    ManchesterList_Ch3 = []
    ManchesterList_Ch4 = []
    ManchesterList_Ch5 = []
    ManchesterList_Ch6 = []
    ManchesterList_Ch7 = []
    
    BC_ID_List = []
    Manchester_Vec_Ch0.clear()
    Manchester_Vec_Ch1.clear()
    Manchester_Vec_Ch2.clear()
    Manchester_Vec_Ch3.clear()
    Manchester_Vec_Ch4.clear()
    Manchester_Vec_Ch5.clear()
    Manchester_Vec_Ch6.clear()
    Manchester_Vec_Ch7.clear()
    TimeVec.clear()
    BC_ID_Vec.clear()
    IsManchester_Vec.clear()
    IsDiscrimatorOR_Vec.clear()

    IsGoodEvent[0] = 0
    Manchester_Ch0[0] = -999
    Manchester_Ch1[0] = -999
    Manchester_Ch2[0] = -999
    Manchester_Ch3[0] = -999
    Manchester_Ch4[0] = -999
    Manchester_Ch5[0] = -999
    Manchester_Ch6[0] = -999 
    Manchester_Ch7[0] = -999
    BC_ID_value[0] = -999
     
    # Read the '.csv' file and save all the infomation into the TTree
    filename = Absolute_DirPath+args.inputDir + '/' +NewFileNameList[i]
    with open(filename) as f:
        f_csv = csv.reader(f)
        headers = next(f_csv)
        headers = next(f_csv)
        for row in f_csv:
            TimeList.append(int(row[0]))
            BC_ID_List.append(row[3])
            ManchesterList_Ch0.append(int(row[12]))
            ManchesterList_Ch1.append(int(row[13]))
            ManchesterList_Ch2.append(int(row[14]))
            ManchesterList_Ch3.append(int(row[15]))
            ManchesterList_Ch4.append(int(row[16]))
            ManchesterList_Ch5.append(int(row[17]))
            ManchesterList_Ch6.append(int(row[18]))
            ManchesterList_Ch7.append(int(row[19]))
            IsManchesterList.append(int(row[20]))
            IsDiscrimatorORList.append(int(row[21]))

    if 0 in IsDiscrimatorORList:
        IsDiscrimatorOR[0] = 1
    else:
        IsDiscrimatorOR[0] = 0
    
    HitTime = -999

    for index in range(len(IsManchesterList)):
        Manchester_Vec_Ch0.push_back(ManchesterList_Ch0[index])
        Manchester_Vec_Ch1.push_back(ManchesterList_Ch1[index])
        Manchester_Vec_Ch2.push_back(ManchesterList_Ch2[index])
        Manchester_Vec_Ch3.push_back(ManchesterList_Ch3[index])
        Manchester_Vec_Ch4.push_back(ManchesterList_Ch4[index])
        Manchester_Vec_Ch5.push_back(ManchesterList_Ch5[index])
        Manchester_Vec_Ch6.push_back(ManchesterList_Ch6[index])
        Manchester_Vec_Ch7.push_back(ManchesterList_Ch7[index])
        TimeVec.push_back(TimeList[index])
        BC_ID_Vec.push_back(int(BC_ID_List[index],16))
        IsManchester_Vec.push_back(IsManchesterList[index])
        IsDiscrimatorOR_Vec.push_back(IsDiscrimatorORList[index])
        if IsManchesterList[index] == 1:      # Change the number according to the test channel numbers
            IsGoodEvent[0] = 1
            HitTime = index
    
    TimeIndex[0] = HitTime
    EventNumber[0] = i
    if HitTime > 0:
        Manchester_Ch0[0] = ManchesterList_Ch0[HitTime]
        Manchester_Ch1[0] = ManchesterList_Ch1[HitTime]
        Manchester_Ch2[0] = ManchesterList_Ch2[HitTime]
        Manchester_Ch3[0] = ManchesterList_Ch3[HitTime]
        Manchester_Ch4[0] = ManchesterList_Ch4[HitTime]
        Manchester_Ch5[0] = ManchesterList_Ch5[HitTime]
        Manchester_Ch6[0] = ManchesterList_Ch6[HitTime]    
        Manchester_Ch7[0] = ManchesterList_Ch7[HitTime]
        BC_ID_value[0] = int(BC_ID_List[HitTime],16)
    MCP_tree.Fill()
 

OutFile.cd()
MCP_tree.Write()
OutFile.Close()    





